import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from '../auth.service';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {BookService} from 'src/app/book.service';
@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books=[];
  nickname;
  user =[];
  bookStatus:boolean;
  text:string;
  writer:string;

  addBook(){
    this.bookService.addBook(this.text,this.writer, this.bookStatus);
    this.text = '';
    this.writer = '';
    this.bookStatus = false;
    
  }



  constructor(private router:Router, private db:AngularFireDatabase, public authService:AuthService, private bookService:BookService) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/user/'+user.uid+'/details').snapshotChanges().subscribe(
        details => {
          details.forEach(
                detail => {
                  let y = detail.payload.toJSON();
                  this.user.push(y);            
                   this.nickname = this.user[0].nickname;         
            }
          )
          
         
        }
      )
      this.db.list('/user/'+user.uid+'/books').snapshotChanges().subscribe(
        books => {
          this.books = [];
          books.forEach(
            book => {
              let b = book.payload.toJSON();
              b['$key'] = book.key;
              this.books.push(b);
  
            }

          )
        }
      )
      
        
      }
     
    )
    
  }

  



}
