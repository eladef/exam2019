import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
password: string;
error='';
output='';


login(){
  this.authService.login(this.email , this.password).then(value=>{
      this.router.navigate(['/books']);
      }).catch(err =>{
        this.error = err.code;
        this.output = err.message;
        console.log(err);
})
}

  constructor(private router:Router, public authService:AuthService ) { }

  ngOnInit() {
  }

}
