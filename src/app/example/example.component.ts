import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from '../auth.service';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';


@Component({
  selector: 'example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.css']
})
export class ExampleComponent implements OnInit {

  items=[];

  constructor(private router:Router, private db:AngularFireDatabase, public authService:AuthService) { }

  ngOnInit() {
    
    this.authService.user.subscribe(user=>{
      if(!user) return;
      this.db.list('/user/' + user.uid + '/items').snapshotChanges().subscribe(
        items => {
          this.items = [];
          items.forEach(
            item => {
              let y = item.payload.toJSON();
              y["$key"] = item.key;
              this.items.push(y);
            }
          )
        }  
      )
    })
  }

}
