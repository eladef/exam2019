import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {Observable} from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  login(email: string , password: string){
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email, password);
  }
  
  logout(){
    return this.fireBaseAuth.auth.signOut()
   }


   signup(email: string , password: string){
    return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  updateProfile(user,name:string){
    user.updateProfile({displayName:name,photoURL:''});
  }
  addUser(user,name : string, email: string, nickname:string){
    let uid = user.uid;
    let ref = this.db.database.ref('/');
    ref.child('user').child(uid).child('details').push({name:name, email: email, nickname:nickname}); 
   }


  user:Observable<firebase.User>;

  constructor(private fireBaseAuth:AngularFireAuth, private db:AngularFireDatabase) {
    this.user =fireBaseAuth.authState;

   }
}
