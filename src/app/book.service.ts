import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {Observable} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  addBook(text:string,writer:string,status:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/user/'+user.uid+'/books').push({'text':text,'writer':writer, 'status':false});
    })
  }
  delete(key){
    this.authService.user.subscribe(user=>{
     this.db.list('user/'+user.uid+'/books').remove(key);
    })
  }

  updateBook(key:string, text:string, writer:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/user/'+user.uid+'/books').update(key,{'text':text,'writer':writer});
    })
  }

  updateStatus(key:string, status:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/user/'+user.uid+'/books').update(key,{'status':status});
    })
    
  }


  constructor(private db:AngularFireDatabase, private authService:AuthService) { }
}
