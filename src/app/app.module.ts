import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // פקודה להבאת הנתיבים

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import { ExampleComponent } from './example/example.component';

//for firebase:
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

//for 2 way bunding:
import{FormsModule} from '@angular/forms';

//for eviromests:
import {environment} from '../environments/environment';

//app modules:
import {MatCardModule} from '@angular/material/card';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ItemComponent } from './item/item.component';
import { RegisterComponent } from './register/register.component';
import { BookComponent } from './book/book.component';
import { BooksComponent } from './books/books.component';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    ExampleComponent,
    ItemComponent,
    RegisterComponent,
    BookComponent,
    BooksComponent,

  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp (environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    FormsModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatButtonModule,
    MatTableModule,
    MatListModule,
    MatInputModule,
    RouterModule.forRoot([
      {path:'', component:BooksComponent},
      {path:'login', component:LoginComponent},
      {path:'book', component:BookComponent},
      {path:'books', component:BooksComponent},
      {path:'example', component:ExampleComponent},
      {path:'register', component:RegisterComponent},
      {path:'**', component:ExampleComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
