import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();

  $key;
  name;


  constructor(private router:Router) { }

  ngOnInit() {
    this.$key = this.data.$key; 
    this.name = this.data.name;
  }

}
