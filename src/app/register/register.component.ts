import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  email : string;
  password : string;
  name : string;
  error= '';
  output='';
  nickname : string;
  required = '';
  check='';
  require = true;
  valid=true;
  checkpassword: string;
  massage='';

  signup(){
    if (this.name == null || this.password == null || this.email == null || this.nickname==null) 
    {
      this.required = "this input is required";
      this.require = false;
    }
    if (this.name != null && this.password != null && this.email != null && this.nickname!=null) 
    {
    
      this.require = true;
    }

                if(this.password!=this.checkpassword)
                {
                  this.check="the passwords are not the same"
                  this.valid=false;
                }
                if(this.password==this.checkpassword){
                  this.valid=true;
                }

                if(this.valid&&this.require){
      this.authService.signup(this.email,this.password).then(value=>{
       this.authService.updateProfile(value.user , this.name);
       this. authService.addUser(value.user,this.name, this.email, this.nickname);
      }).then(value=>{
        this.router.navigate(['/books']);
      }).catch(err =>{
        this.error = err.code;
        this.output = err.message;
        console.log(this.email);

      })
    }
 
          }


  constructor(private authService:AuthService , private router:Router) { }

  ngOnInit() {
  }

}
