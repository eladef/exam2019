import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from "@angular/router";
import { BookService } from 'src/app/book.service';
@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();

  $key;
  name;
  text;
  writer;
  showTheButton = false;

showEditField = false;
tempWriter;
tempText;
comment;
id;
key;
status: boolean;

  showButton(){
    this.showTheButton=true;
    }
    hideButton(){
      this.showTheButton=false;
      }
      delete(){
        this.bookService.delete(this.$key);
      }

      showEdit()
{
  this.tempText = this.text;
  this.tempWriter = this.writer;
  this.showTheButton = false;
  this.showEditField = true;
}
     

save()
{
  this.bookService.updateBook(this.key,this.text,this.writer);
  this.showEditField = false;
}

cancel()
{
  this.text = this.tempText;
  this.writer = this.tempWriter;
  this.showEditField = false;
}

send(){

  this.myButtonClicked.emit(this.text);

    }

    checkChange()
    {
    
      this.bookService.updateStatus(this.$key,this.status);
    }


  constructor(private router:Router, private bookService:BookService) { }

  ngOnInit() {
    this.$key = this.data.$key; 
    this.name = this.data.name;
    this.text = this.data.text;
    this.writer = this.data.writer;

  }

}
